﻿using Assets.scripts;
using Assets.scripts.models;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{

    private List<List<Item>> _gameGrid;
    private float _koef;
    private bool _gameStarted = false;
    private List<Item> _allItems;
    private List<Item> twoOpenedItem = new List<Item>();
    private int _score;


    public Text ScoreTxt;
    public Camera MainCamera;
    public GameObject GameBoard;
    private DeviceOrientation _previosDeviseOrientVal;

    IEnumerator Start()
    {
        if (GameResourcess.AtlassTexture == null)
        {
            string url = GameResourcess.lvl1;
            WWW loadLevel = new WWW(url);
            yield return loadLevel;
            if (loadLevel.error == null)
            {
                GameResourcess.Level = JsonUtility.FromJson<Level>(loadLevel.text);
                GameResourcess.MAX_DIFF_ITEMS = GameResourcess.Level.picturesPos.Count;
                GameResourcess.GridSizeX = GameResourcess.Level.GridSizeX;
                GameResourcess.GridSizeY = GameResourcess.Level.GridSizeY;
            }
            else
            {
                Debug.Log("ERROR: " + loadLevel.error);
            }

            WWW loadAtlass = new WWW(GameResourcess.Level.altlasUrl);
            while (!loadAtlass.isDone)
                yield return null;
            GameResourcess.AtlassTexture = loadAtlass.texture;
        }

        InitializeGrid();
        RescaleGrid();

        StartCoroutine(DoAfterSomeTime(RotateAllBack, 5f));
    }

    IEnumerator DoAfterSomeTime(Action action, float seconds)
    {
            yield return new WaitForSeconds(seconds);
        action.Invoke();
    }

    void RotateAllBack()
    {
        for (var i = 0;  i < _allItems.Count; i++)
        {
            _allItems[i].RotateBack();
        }
        _gameStarted = true;
    }

    private void RescaleGrid()
    {
        if (GameResourcess.AllItemsWIdth > Screen.width/100)
        {
            _koef = (Screen.width / 100)/(GameResourcess.AllItemsWIdth);
            GameBoard.transform.localScale = new Vector3(_koef, _koef, _koef);
        }
    }

    private void InitializeGrid()
    {
        GameResourcess.AllItemsWIdth = Convert.ToSingle(((GameResourcess.ITEM_SIZE + 0.01) * GameResourcess.GridSizeX));
        GameResourcess.AllItemsHeigth = Convert.ToSingle(((GameResourcess.ITEM_SIZE + 0.01) * GameResourcess.GridSizeY));

        ///Resource pool
        List<UnityEngine.Object> loadedResourcess = new List<UnityEngine.Object>();
        for (int i = 0; i < GameResourcess.MAX_DIFF_ITEMS; i++)
        {
            string prefubName = "prefabs/atlas0_" + i.ToString();
            var loadedObj = Resources.Load(prefubName);
            if (loadedObj != null)
            {
                loadedResourcess.Add(loadedObj);
            }
        }
        loadedResourcess.Shuffle();
        ///


        List<GameObject> firstPairItems = new List<GameObject>();
        _allItems = new List<Item>();
        _gameGrid = new List<List<Item>>();
        for (int i = 0; i < GameResourcess.GridSizeX; i++)
        {
            _gameGrid.Add(new List<Item>());
            for (int j = 0; j < GameResourcess.GridSizeY; j++)
            {
                Item item;
                GameObject initedPrefab = null;

                ///Add second element of pair for exist on board element
                if (_allItems.Count >= (GameResourcess.GridSizeX * GameResourcess.GridSizeY) / 2)
                {
                    var randomeItemOfPair = UnityEngine.Random.Range(0, firstPairItems.Count);  //get random element for board
                    initedPrefab = firstPairItems[randomeItemOfPair];

                    firstPairItems.RemoveAt(randomeItemOfPair);
                }
                ///


                ///Add first element of pair and preparing pool of second pair elements
                if (_allItems.Count < (GameResourcess.GridSizeX * GameResourcess.GridSizeY) / 2)
                {
                    var itemTypeNum = 0;
                    if (_allItems.Count < GameResourcess.MAX_DIFF_ITEMS)   //all different items on board 100% but always in different place because resource shuffled
                    {
                        itemTypeNum = _allItems.Count;
                    }
                    else
                    {
                        itemTypeNum = UnityEngine.Random.Range(0, GameResourcess.MAX_DIFF_ITEMS);
                    }
                       
                    initedPrefab = Instantiate(loadedResourcess[itemTypeNum]) as GameObject;

                    var curentSprite = Sprite.Create(GameResourcess.AtlassTexture, 
                                                      new Rect(GameResourcess.Level.picturesPos[itemTypeNum].posX,
                                                             GameResourcess.Level.picturesPos[itemTypeNum].posY,
                                                             GameResourcess.Level.picturesPos[1].posX, //item width
                                                             GameResourcess.AtlassTexture.height),      //item heigth
                                                                  new Vector2(0.5f, 0.5f));
                    initedPrefab.GetComponent<SpriteRenderer>().sprite = curentSprite;
                    
                    
                    var pairForInitedPrefab = Instantiate(loadedResourcess[itemTypeNum]) as GameObject;
                    pairForInitedPrefab.GetComponent<SpriteRenderer>().sprite = curentSprite;

                    initedPrefab.name = pairForInitedPrefab.name = "item_" + i.ToString() + "_" + j.ToString();
                    firstPairItems.Add(pairForInitedPrefab);
                }
                ///

                item = new Item(i, j, initedPrefab);
                _gameGrid[i].Add(item);
                _allItems.Add(item);

                initedPrefab.transform.SetParent(GameBoard.transform);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && _gameStarted)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.transform != null)
            {
                if (twoOpenedItem.Count < 2)
                {
                    Item item = GetItemByTransform(hit.transform);
                    if (item != null)
                    {
                        item.ItemSelected = true;
                        twoOpenedItem.Add(item);
                        item.Rotate();
                        if (twoOpenedItem.Count > 1)
                        {
                            StartCoroutine(DoAfterSomeTime(MatchResult, 2f));
                        }
                    }
                }
            }
        }

        if (twoOpenedItem.Count == 2) //if items is different rotate back
        {
            if (twoOpenedItem[0].Name != twoOpenedItem[1].Name)
            {
                StartCoroutine(DoAfterSomeTime(RotateOpenedBack, 2.2f));
            }
        }

        if (_previosDeviseOrientVal != Input.deviceOrientation)
        {
            if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
            {
                RescaleGrid();
            }
            else if (Input.deviceOrientation == DeviceOrientation.Portrait)
            {
                RescaleGrid();
            }
        }
        _previosDeviseOrientVal = Input.deviceOrientation;
    }

    private void MatchResult()
    {
       if (twoOpenedItem.Count > 1 && twoOpenedItem[0].Name == twoOpenedItem[1].Name)
        {
            for (var i = 0; i < twoOpenedItem.Count; i++)
            {
                twoOpenedItem[i].Hide();
                twoOpenedItem[i].ItemSelected = false;
                _allItems.Remove(twoOpenedItem[i]);
            }
            twoOpenedItem.Clear();

            _score++;
            ScoreTxt.text = _score.ToString();
            if (_allItems.Count == 0)
            {
                StartCoroutine(DoAfterSomeTime(RestartLevel, 2f));
            }

        }
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene("Game");
    }

    private void RotateOpenedBack()
    {
        for (var i = 0; i < twoOpenedItem.Count; i++)
        {
            twoOpenedItem[i].RotateBack();
            twoOpenedItem[i].ItemSelected = false;
        }
        twoOpenedItem.Clear();
    }
    private Item GetItemByTransform(Transform transform)
    {
        Item item = _allItems.Find(x => x.ItemGameObject.transform == transform && x.ItemSelected == false);
        return item;
    }
}
