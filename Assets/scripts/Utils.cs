﻿using Assets.scripts.models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Random = System.Random;

namespace Assets.scripts
{
	public static class Utils
	{
		public static Rect Get_Rect(GameObject gameObject)
		{
			if (gameObject != null)
			{
				RectTransform rectTransform = gameObject.GetComponent<RectTransform>();

				if (rectTransform != null)
				{
					return rectTransform.rect;
				}
			}
			else
			{
				Debug.Log("Game object is null.");
			}

			return new Rect();
		}

		private static Random rng = new Random();

		public static void Shuffle<T>(this IList<T> list)
		{
			int n = list.Count;
			while (n > 1)
			{
				n--;
				int k = rng.Next(n + 1);
				T value = list[k];
				list[k] = list[n];
				list[n] = value;
			}
		}

		public static IEnumerator ExecuteAfterTime(float time, Action task)
		{
			yield return new WaitForSeconds(time);
			task();
		}
	}
}
