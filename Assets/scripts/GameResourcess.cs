﻿using Assets.scripts.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.scripts
{
    public static class GameResourcess
    {
        public static string lvl1 = "https://drive.google.com/uc?id=1pHh3tVmm6gHufZJV64wDDDu8DOUKhdor";
        public static int MAX_DIFF_ITEMS = 3;
        public const float ITEM_SIZE = 2.14f;

        public static int GridSizeX = 3;
        public static int GridSizeY = 2;
        
        public static float AllItemsWIdth = 0;
        public static float AllItemsHeigth = 0;

        public static Level Level { get; internal set; }
        public static Texture2D AtlassTexture { get; internal set; }
    }
}
