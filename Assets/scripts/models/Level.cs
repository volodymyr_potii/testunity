﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.scripts.models
{
    [Serializable]
    public class Level
    {
        public List<PictPos> picturesPos;

        public String altlasUrl;

        public int GridSizeX;

        public int GridSizeY;
    }
}
