﻿using Assets.scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public Item(int i, int j, GameObject itemGameObject)
    {
        if (itemGameObject != null)
        {
            ItemGameObject = itemGameObject;
            Name = ItemGameObject.name;
        }


        PosX = Convert.ToSingle(i * GameResourcess.ITEM_SIZE + 0.01 - GameResourcess.AllItemsWIdth / 2 + GameResourcess.ITEM_SIZE / 2);
        PosY = Convert.ToSingle(j * GameResourcess.ITEM_SIZE + 0.01 - GameResourcess.AllItemsHeigth / 2 + GameResourcess.ITEM_SIZE / 2);
        ItemGameObject.transform.position = new Vector3(PosX, PosY);
    }

     public bool RotateFinished {get; set;}

     public bool ItemSelected { get; set; }

     public bool ItemUnselected { get; set; }

    public string Name { get; set; }

     public GameObject ItemGameObject { get; set; }

     public float PosX { get; set; }

     public float PosY { get; set; }

    public void Rotate()
    {
        ItemUnselected = false;
        ItemSelected = true;
        ItemGameObject.GetComponent<ItemScript>().UpdateItem(this);
    }

    public void RotateBack()
    {
        ItemUnselected = true;
        ItemSelected = false;
        ItemGameObject.GetComponent<ItemScript>().UpdateItem(this);
    }

    internal void Hide()
    {
        ItemGameObject.SetActive(false);
    }

    internal void Show()
    {
        ItemGameObject.SetActive(true);
    }

    internal void ResetItem()
    {
        Show();
        ItemGameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
    }
}
