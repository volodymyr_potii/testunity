﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.scripts.models
{
    [Serializable]
    public class PictPos
    {
        public int posX;

        public int posY;

        public string name;
    }
}
