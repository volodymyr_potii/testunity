﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour
{

    private  Item _gameItem;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameItem != null)
        {
            float degrees = 0;
            if (_gameItem.ItemSelected)
            {
               degrees = 0;
            }
            else if (_gameItem.ItemUnselected)
            {
               degrees = 180;
            }
            Vector3 to = new Vector3(0, degrees, 0);

            transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, to, Time.deltaTime);
        }

    }

    public void UpdateItem(Item item)
    {
        _gameItem = item;
    }
}
